//
//  StoreCollectionViewCell.swift
//  Mono
//
//  Created by gusgus on 11/3/2561 BE.
//  Copyright © 2561 gusgus. All rights reserved.
//

import UIKit

class StoreCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgImage: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPublisher: UILabel!
    
    @IBAction func AddtoCart(_ sender: Any) {
    }
    
    func setValue(DataBook : DataBook){
        lblName.text! = DataBook.Name
        lblPrice.text! = DataBook.Price+" ฿"
        lblPublisher.text! = DataBook.Publisher
        
        
        if DataBook.PictureImg != nil{
            imgImage.image = DataBook.PictureImg
        }
        else{
            GetPictureImg(DataBook: DataBook)
        }
        
    }
    
    func GetPictureImg(DataBook : DataBook){
//        let url = URL(string: DataBook.BookImage)
//        let data = try? Data(contentsOf: url!)
//        DataBook.PictureImg = UIImage(data: data!)
//        
//        self.imgImage.image = DataBook.PictureImg
        
        DispatchQueue.global(qos: .userInitiated).async {
            let url = URL(string: DataBook.BookImage)
            let data = try? Data(contentsOf: url!)
            DataBook.PictureImg = UIImage(data: data!)
            // Bounce back to the main thread to update the UI
            DispatchQueue.main.async {
                self.imgImage.image = DataBook.PictureImg
            }
        }
    }
}
