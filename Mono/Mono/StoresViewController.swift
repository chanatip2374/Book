//
//  StoreViewController.swift
//  Mono
//
//  Created by gusgus on 11/3/2561 BE.
//  Copyright © 2561 gusgus. All rights reserved.
//

import UIKit

class StoresViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var allData = [DataBook]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if Reachability.isConnectToNetwork() == true{
            let api = APIFunc()
            api.callAPI(urlString: "http://store.mbookstore.com/service?params=home&limit_start=0&limit_stop=100", completion: callAPIFinish)
//        }
//            //Can't connect to Internet
//        else{
//            let failedStatus = "Internet Connection Failed"
//            print(failedStatus)
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desVC = mainStoryboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        self.navigationController?.pushViewController(desVC, animated: true)
        let allBook = allData[indexPath.row]
        desVC.allBook = allBook
    }
    
    
    func callAPIFinish(DataBookArray : [DataBook]){
        self.allData = DataBookArray
        collectionView.reloadData()
        
//        for Books in DataBookArray{
//            print(Books.Id+":"+Books.Name+)
//        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreCollectionViewCell", for: indexPath) as! StoreCollectionViewCell
        
        let bookArray = allData[indexPath.row]
        cell.setValue(DataBook: bookArray)
        
        return cell
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
