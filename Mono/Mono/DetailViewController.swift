//
//  DetailViewController.swift
//  Mono
//
//  Created by gusgus on 12/3/2561 BE.
//  Copyright © 2561 gusgus. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate{
    
    

    @IBOutlet weak var imgImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPublisher: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    var iconImage = ["home","login","package","userguide","about"]
    var allBook : DataBook!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.title = allBook.Name
        lblName.text = allBook.Name
        lblPublisher.text = "สำนักพิมพ์ : "+allBook.Publisher
        lblPrice.text = allBook.Price+" บาท"
        if allBook.PictureImg != nil{
            imgImage.image = allBook.PictureImg
        }
        // Do any additional setup after loading the view.
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return iconImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RelateBookCollectionViewCell", for: indexPath) as! RelateBookCollectionViewCell
        cell.iconImage.image = UIImage(named: iconImage[indexPath.row]+".PNG")

        
        return cell
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
