//
//  DataBook.swift
//  Mono
//
//  Created by gusgus on 11/3/2561 BE.
//  Copyright © 2561 gusgus. All rights reserved.
//

import Foundation
import UIKit
class DataBook{
    private var _id: String!
    private var _name: String!
    private var _publisher:String!
    private var _price:String!
    private var _tagname:String!
    private var _tagcolor:String!
    private var _tagcolor2:String!
    private var _booktotal:String!
    private var _bookimage:String!
    private var _instock:String!
    private var _pictureImg: UIImage!
    
    var Id: String{
        return _id
    }
    var Name: String{
        return _name
    }
    var Publisher: String{
        return _publisher
    }
    var Price: String{
        return _price
    }
    var TagName: String{
        return _tagname
    }
    var TagColor: String{
        return _tagcolor
    }
    var TagColor2: String{
        return _tagcolor2
    }
    var BookTotal: String{
        return _booktotal
    }
    var BookImage: String{
        return _bookimage
    }
    var InStock: String{
        return _instock
    }
    var PictureImg: UIImage!{
        get {return _pictureImg}
        set {_pictureImg = newValue}
    }
    
    init(id:String,name:String,publisher:String,price:String,tagname:String,tagcolor:String,tagcolor2:String,booktotal:String,bookimage:String,instock:String){
        self._id = id
        self._name = name
        self._publisher = publisher
        self._price = price
        self._tagname = tagname
        self._tagcolor = tagcolor
        self._tagcolor2 = tagcolor2
        self._booktotal = booktotal
        self._bookimage = bookimage
        self._instock = instock
        self._pictureImg = nil
        
    }
}
