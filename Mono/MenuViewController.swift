//
//  MenuViewController.swift
//  Mono
//
//  Created by gusgus on 9/3/2561 BE.
//  Copyright © 2561 gusgus. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var menuNameArr = [String]()
    var iconImage = [String]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        menuNameArr = ["Home","Login","Package","User Guide","About"]
        iconImage = ["home","login","package","userguide","about"]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        
//        cell.imgIcon.image = iconImage[indexPath.row]
        cell.lblMenuName.text! = menuNameArr[indexPath.row]
        cell.imgIcon.image = UIImage(named: iconImage[indexPath.row]+".PNG")
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let revalViewController:SWRevealViewController = self.revealViewController()
        
        let cell:MenuTableViewCell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        if cell.lblMenuName.text! == "Home"
        {
            let mainStroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStroyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            
            let newFrontViewController = UINavigationController.init(rootViewController: desController)
            
            revalViewController.pushFrontViewController(newFrontViewController, animated: true)
        }
        if cell.lblMenuName.text! == "Login"
        {
            let mainStroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStroyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            
            let newFrontViewController = UINavigationController.init(rootViewController: desController)
            
            revalViewController.pushFrontViewController(newFrontViewController, animated: true)
        }
        if cell.lblMenuName.text! == "Package"
        {
            let mainStroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStroyboard.instantiateViewController(withIdentifier: "PackageViewController") as! PackageViewController
            
            let newFrontViewController = UINavigationController.init(rootViewController: desController)
            
            revalViewController.pushFrontViewController(newFrontViewController, animated: true)
        }
        if cell.lblMenuName.text! == "User Guide"
        {
            let mainStroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStroyboard.instantiateViewController(withIdentifier: "UserguideViewController") as! UserguideViewController
            
            let newFrontViewController = UINavigationController.init(rootViewController: desController)
            
            revalViewController.pushFrontViewController(newFrontViewController, animated: true)
        }
        if cell.lblMenuName.text! == "About"
        {
            let mainStroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStroyboard.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
            
            let newFrontViewController = UINavigationController.init(rootViewController: desController)
            
            revalViewController.pushFrontViewController(newFrontViewController, animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
