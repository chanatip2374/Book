//
//  APIFunc.swift
//  Mono
//
//  Created by gusgus on 9/3/2561 BE.
//  Copyright © 2561 gusgus. All rights reserved.
//

import Foundation
class APIFunc{
    func callAPI(urlString: String,completion:@escaping (_ DataBookArray: [DataBook]) -> Void){
        
        let session = URLSession.shared
        let url = URL(string: urlString)!
        let task = session.dataTask(with: url) { (data, response, error) in
            if error != nil{
                DispatchQueue.global(qos: .background).async {
                    // Background Thread
                    DispatchQueue.main.async {
                        print(error!.localizedDescription)
                        // Run UI Updates or call completion block
                    }
                }
            }
            else{
                if let returnData = data{
                    do{
                        if let jsonObject = try JSONSerialization.jsonObject(with: returnData, options: .allowFragments) as? [String: AnyObject],let status = jsonObject["status"] as? String, let data = jsonObject["data"] as? [String: AnyObject],let bookArray = data["book"] as? Array<AnyObject>,let bannerArray = data["banner"] as? Array<AnyObject>
                        {
                            var id = ""
                            var name = ""
                            var publisher = ""
                            var price = ""
                            var tagname = ""
                            var tagname_2 = ""
                            var tagcolor = ""
                            var tagcolor_2 = ""
                            var booktotal = ""
                            var bookimage = ""
                            var instock = ""
                            var AllBookArray = [DataBook]()
                            for books in bookArray{
                                if let book_id = books["book_id"] as? String
                                    , let book_name = books["book_name"] as? String
                                    , let publisher_name = books["publisher_name"] as? String
                                    ,let book_price = books["book_price"] as? String
                                    , let tag_name = books["tag_name"] as? String
                                    , let tag_color = books["tag_color"] as? String
                                    , let tag_color_2 = books["tag_color_2"] as? String
                                    , let book_total = books["book_total"] as? String
                                    , let book_image = books["book_image"] as? String
                                    , let in_stock = books["in_stock"] as? String
                                {
                                    
                                    id = book_id
                                    name = book_name
                                    publisher = publisher_name
                                    price = book_price
                                    tagname = tag_name
                                    tagcolor = tag_color
                                    tagcolor_2 = tag_color_2
                                    booktotal = book_total
                                    bookimage = book_image
                                    instock = in_stock
                                }
                                
                                let resultBook = DataBook(id: id, name: name, publisher: publisher, price: price, tagname: tagname, tagcolor: tagcolor, tagcolor2: tagcolor_2, booktotal: booktotal, bookimage: bookimage, instock: instock)
                                AllBookArray.append(resultBook)
                                
                            }
                            DispatchQueue.global(qos: .background).async {
                                // Background Thread
                                DispatchQueue.main.async {
                                    completion(AllBookArray)
                                    // Run UI Updates or call completion block
                                }
                            }
                        }

                        
                        
                    }
                    catch{
                        print("JSONSerialization error")
                    }
                }
                else{
                    print("Call API Error")
                    
                }
            }
            
        }
        task.resume()
    }
}
